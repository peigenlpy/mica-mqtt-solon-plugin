# mica-mqtt-solon-plugin
本插件是完全基于[mica-mqtt](https://gitee.com/596392912/mica-mqtt)，是适用于[Solon](https://gitee.com/noear/solon)的插件。

开源项目不容易，如果觉得本项目对您的工作还是有帮助的话，请在帮忙在[mica-mqtt-solon-plugin](https://gitee.com/peigenlpy/mica-mqtt-solon-plugin)点一下Star(页面右上角)，谢谢。


简单介绍下本插件在solon工程下如何使用：
## 首先
在项目工程pom.xml里加入Maven依赖：


``` xml
<!-- Server -->
<dependency>
    <groupId>com.gitee.peigenlpy</groupId>
    <artifactId>mica-mqtt-server-solon-plugin</artifactId>
    <version>2.2.4</version>
</dependency>

<!-- Client -->
<dependency>
    <groupId>com.gitee.peigenlpy</groupId>
    <artifactId>mica-mqtt-client-solon-plugin</artifactId>
    <version>2.2.4</version>
</dependency>
```

## 其次

在项目配置文件app.yml下配置（也可以不配置，使用默认值）

Server端：
``` xml
# mqtt 服务端配置
mqtt:
  server:
    enabled: true               # 是否开启服务端，默认：true
    ip: 0.0.0.0                # 服务端 ip 默认为空，0.0.0.0，建议不要设置
    port: 18889                  # 端口，默认：1883
    name: Mica-Mqtt-Server      # 名称，默认：Mica-Mqtt-Server
    buffer-allocator: HEAP      # 堆内存和堆外内存，默认：堆内存
    heartbeat-timeout: 120000   # 心跳超时，单位毫秒，默认: 1000 * 120
    read-buffer-size: 8KB       # 接收数据的 buffer size，默认：8k
    max-bytes-in-message: 10MB  # 消息解析最大 bytes 长度，默认：10M
    auth:
      enable: false             # 是否开启 mqtt 认证
      username: mica            # mqtt 认证用户名
      password: mica            # mqtt 认证密码
    debug: true                 # 如果开启 prometheus 指标收集建议关闭
    stat-enable: true           # 开启指标收集，debug 和 prometheus 开启时需要打开，默认开启，关闭节省内存
    web-port: 8083              # http、websocket 端口，默认：8083
    websocket-enable: true      # 是否开启 websocket，默认： true
    http-enable: false          # 是否开启 http api，默认： false
    http-basic-auth:
      enable: false             # 是否开启 http basic auth，默认： false
      username: mica            # http basic auth 用户名
      password: mica            # http basic auth 密码
    ssl:                        # mqtt tcp ssl 认证
      enabled: false            # 是否开启 ssl 认证，2.1.0 开始支持双向认证
      keystore-path:            # 必须参数：ssl keystore 目录，支持 classpath:/ 路径。
      keystore-pass:            # 必选参数：ssl keystore 密码
      truststore-path:          # 可选参数：ssl 双向认证 truststore 目录，支持 classpath:/ 路径。
      truststore-pass:          # 可选参数：ssl 双向认证 truststore 密码
      client-auth: none         # 是否需要客户端认证（双向认证），默认：NONE（不需要）
```

Client端：
``` xml
#mqtt-client 配置
mqtt:
  client:
    enabled: true               # 是否开启客户端，默认：true
    ip: 127.0.0.1               # 连接的服务端 ip ，默认：127.0.0.1
    port: 18889                  # 端口：默认：1883
    name: Mica-Mqtt-Client      # 名称，默认：Mica-Mqtt-Client
    clientId: 000001            # 客户端Id（非常重要，一般为设备 sn，不可重复）
    user-name: mica             # 认证的用户名
    password: mica            # 认证的密码
    timeout: 5                  # 超时时间，单位：秒，默认：5秒
    reconnect: true             # 是否重连，默认：true
    re-interval: 5000           # 重连时间，默认 5000 毫秒
    version: mqtt_3_1_1         # mqtt 协议版本，可选 MQTT_3_1、mqtt_3_1_1、mqtt_5，默认：mqtt_3_1_1
    read-buffer-size: 8KB       # 接收数据的 buffer size，默认：8k
    max-bytes-in-message: 10MB  # 消息解析最大 bytes 长度，默认：10M
    buffer-allocator: heap      # 堆内存和堆外内存，默认：堆内存
    keep-alive-secs: 60         # keep-alive 时间，单位：秒
    clean-session: true         # mqtt clean session，默认：true
    ssl:
      enabled: false            # 是否开启 ssl 认证，2.1.0 开始支持双向认证
      keystore-path:            # 可选参数：ssl 双向认证 keystore 目录，支持 classpath:/ 路径。
      keystore-pass:            # 可选参数：ssl 双向认证 keystore 密码
      truststore-path:          # 可选参数：ssl 双向认证 truststore 目录，支持 classpath:/ 路径。
      truststore-pass:          # 可选参数：ssl 双向认证 truststore 密码

topic1: /test2/#
```

到这里已经完成mica-mqtt从spring到solon的转变了。以下的部分就和原版一致了，可以直接看mica-mqtt的官方文档：
[mica-mqtt-server官方文档地址](https://gitee.com/596392912/mica-mqtt/tree/master/starter/mica-mqtt-server-spring-boot-starter)，
[mica-mqtt-client官方文档地址](https://gitee.com/596392912/mica-mqtt/tree/master/starter/mica-mqtt-client-spring-boot-starter)。







