/* Copyright (c) 2022 Peigen.info. All rights reserved. */

package com.gitee.peigenlpy.mica.server.config;

import cn.hutool.core.util.ObjUtil;
import com.gitee.peigenlpy.mica.server.MqttServerCustomizer;
import com.gitee.peigenlpy.mica.server.MqttServerTemplate;
import com.gitee.peigenlpy.mica.server.event.SolonEventMqttConnectStatusListener;
import com.gitee.peigenlpy.mica.server.event.SolonEventMqttMessageListener;
import net.dreamlu.iot.mqtt.core.server.MqttServer;
import net.dreamlu.iot.mqtt.core.server.MqttServerCreator;
import net.dreamlu.iot.mqtt.core.server.auth.IMqttServerAuthHandler;
import net.dreamlu.iot.mqtt.core.server.auth.IMqttServerPublishPermission;
import net.dreamlu.iot.mqtt.core.server.auth.IMqttServerSubscribeValidator;
import net.dreamlu.iot.mqtt.core.server.auth.IMqttServerUniqueIdService;
import net.dreamlu.iot.mqtt.core.server.dispatcher.IMqttMessageDispatcher;
import net.dreamlu.iot.mqtt.core.server.event.IMqttConnectStatusListener;
import net.dreamlu.iot.mqtt.core.server.event.IMqttMessageListener;
import net.dreamlu.iot.mqtt.core.server.event.IMqttSessionListener;
import net.dreamlu.iot.mqtt.core.server.interceptor.IMqttMessageInterceptor;
import net.dreamlu.iot.mqtt.core.server.session.IMqttSessionManager;
import net.dreamlu.iot.mqtt.core.server.store.IMqttMessageStore;
import net.dreamlu.iot.mqtt.core.server.support.DefaultMqttServerAuthHandler;
import org.noear.solon.Solon;
import org.noear.solon.annotation.*;
import org.noear.solon.lang.Nullable;

/**
 * <b>(MqttServerConfiguration)</b>
 *
 * @author LiHai
 * @version 1.0.0
 * @since 2023/7/20
 */
@Configuration
//@Import(scanPackages = {"com.gitee.peigenlpy.mica"}) //todo: 看上去这个导入扫描没必要，用  context.beanMake(MqttServerProperties.class); 性能更好。by noear,2023-09-15
public class MqttServerConfiguration {

    @Bean
    @Condition(onMissingBean = IMqttConnectStatusListener.class)
    public IMqttConnectStatusListener connectStatusListener() {
        return new SolonEventMqttConnectStatusListener();
    }

    @Bean
    @Condition(onMissingBean = IMqttMessageListener.class)
    public IMqttMessageListener messageListener() {
        return new SolonEventMqttMessageListener();
    }

    @Bean
    public MqttServerCreator mqttServerCreator(MqttServerProperties properties) {
        MqttServerCreator serverCreator = MqttServer.create()
                .name(properties.getName())
                .ip(properties.getIp())
                .port(properties.getPort())
                .heartbeatTimeout(properties.getHeartbeatTimeout())
                .keepaliveBackoff(properties.getKeepaliveBackoff())
                .readBufferSize((int) properties.getReadBufferSize().toBytes())
                .maxBytesInMessage((int) properties.getMaxBytesInMessage().toBytes())
                .bufferAllocator(properties.getBufferAllocator())
                .maxClientIdLength(properties.getMaxClientIdLength())
                .webPort(properties.getWebPort())
                .websocketEnable(properties.isWebsocketEnable())
                .httpEnable(properties.isHttpEnable())
                .nodeName(properties.getNodeName())
                .statEnable(properties.isStatEnable());
        if (properties.isDebug()) {
            serverCreator.debug();
        }

        // http 认证
        MqttServerProperties.HttpBasicAuth httpBasicAuth = properties.getHttpBasicAuth();
        if (serverCreator.isHttpEnable() && httpBasicAuth.isEnable()) {
            serverCreator.httpBasicAuth(httpBasicAuth.getUsername(), httpBasicAuth.getPassword());
        }
        MqttServerProperties.Ssl ssl = properties.getSsl();
        // ssl 配置
        if (ssl.isEnabled()) {
            serverCreator.useSsl(ssl.getKeystorePath(), ssl.getKeystorePass(), ssl.getTruststorePath(), ssl.getTruststorePass(), ssl.getClientAuth());
        }
        return serverCreator;
    }

}
